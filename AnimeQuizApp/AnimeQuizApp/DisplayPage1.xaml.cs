﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using AnimeQuizApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AnimeSearch.SearchModel;
using AnimeQuizApp.Models;

namespace AnimeQuizApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DisplayPage1 : ContentPage
	{
        public DisplayPage1 (Welcome animeObject, AnimeByIdClass animeObject2)
		{
			InitializeComponent ();
            DisplayTitle.Text = animeObject.Result[0].Title;
            Uri uri = new Uri(animeObject2.ImageUrl);
            AnimeImage.Source = ImageSource.FromUri(uri);
            DisplaySynopsis.Text = animeObject.Result[0].Description;
		}
	}
}