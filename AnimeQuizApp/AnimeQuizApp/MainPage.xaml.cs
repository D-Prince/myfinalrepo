﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Plugin.Connectivity;
using AnimeQuizApp.Models;
using Xamarin.Forms;
using Newtonsoft.Json;
using AnimeSearch.SearchModel;


namespace AnimeQuizApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            UpdateConnectionLabel();
            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                UpdateConnectionLabel();
            };
        }
        private void UpdateConnectionLabel()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    DisplayAlert("Alert", "You are not connected to internet", "OK");
                });
            }
        }


        async void QueryButtonClicked(Object sender, System.EventArgs e)
        {
            Random rnd = new Random();
            bool success = false;
            do
            {
                var client = new HttpClient(new NativeMessageHandler());
                /* String word = WordLabel.Text.ToString();
                 if (word.Length <= 0)
                 {
                     return;
                 }
                 if (word.IndexOf(" ") != -1)//if word entered is more than one word
                 {
                     word = word.Substring(0, word.IndexOf(" "));//take the first word
                 }
                 */
                
                int randNum = rnd.Next(1, 3516);
                String randS = randNum.ToString();
                var uri = new Uri(
                    string.Format($"https://api.jikan.moe/search/anime/Baccano/1"));
                var uri2 = new Uri(
                    string.Format($"https://api.jikan.me/anime/2251"));
                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");
                var request2 = new HttpRequestMessage();
                request2.Method = HttpMethod.Get;
                request2.RequestUri = uri2;
                request2.Headers.Add("Application", "application / json");

                Welcome animeObject;
                AnimeByIdClass animeObject2;
                //SearchAnimeNameList searchAnimeNameListObject;
                HttpResponseMessage response; response = await client.SendAsync(request);
                HttpResponseMessage response2; response2 = await client.SendAsync(request2);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //searchAnimeName= SearchAnimeNameList.FromJson(content);
                    animeObject = Welcome.FromJson(content);
                    animeObject2 = AnimeByIdClass.FromJson(content);
                    await Navigation.PushAsync(new DisplayPage1(animeObject,animeObject2));
                    success = true;
                }

                //if (response.IsSuccessStatusCode)
                //{
                //    var content = await response.Content.ReadAsStringAsync();
                //    //searchAnimeName= SearchAnimeNameList.FromJson(content);
                //    animeObject2 = Welcome.FromJson(content);
                //    await Navigation.PushAsync(new DisplayPage1(animeObject2));
                //    success = true;
                //}
            }while(!success);
        }

        //void Handle_Clicked(object sender, System.EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
