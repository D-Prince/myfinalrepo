﻿namespace AnimeSearch.SearchModel
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Welcome
    {
        [JsonProperty("request_hash")]
        public string RequestHash { get; set; }

        [JsonProperty("request_cached")]
        public bool RequestCached { get; set; }

        [JsonProperty("result")]
        public Result[] Result { get; set; }

        [JsonProperty("result_last_page")]
        public long ResultLastPage { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("mal_id")]
        public long MalId { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("image_url")]
        public string ImageUrl { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("type")]
        public TypeEnum Type { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        [JsonProperty("episodes")]
        public long Episodes { get; set; }

        [JsonProperty("members")]
        public long Members { get; set; }
    }

    public enum TypeEnum { Movie, Music, Ona, Ova, Special, Tv };

    public partial class Welcome
    {
        public static Welcome FromJson(string json) => JsonConvert.DeserializeObject<Welcome>(json, AnimeSearch.SearchModel.Converter.Settings);
    }

    static class TypeEnumExtensions
    {
        public static TypeEnum? ValueForString(string str)
        {
            switch (str)
            {
                case "Movie": return TypeEnum.Movie;
                case "Music": return TypeEnum.Music;
                case "ONA": return TypeEnum.Ona;
                case "OVA": return TypeEnum.Ova;
                case "Special": return TypeEnum.Special;
                case "TV": return TypeEnum.Tv;
                default: return null;
            }
        }

        public static TypeEnum ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this TypeEnum value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case TypeEnum.Movie: serializer.Serialize(writer, "Movie"); break;
                case TypeEnum.Music: serializer.Serialize(writer, "Music"); break;
                case TypeEnum.Ona: serializer.Serialize(writer, "ONA"); break;
                case TypeEnum.Ova: serializer.Serialize(writer, "OVA"); break;
                case TypeEnum.Special: serializer.Serialize(writer, "Special"); break;
                case TypeEnum.Tv: serializer.Serialize(writer, "TV"); break;
            }
        }
    }

    public static class Serialize
    {
        public static string ToJson(this Welcome self) => JsonConvert.SerializeObject(self, AnimeSearch.SearchModel.Converter.Settings);
    }

    internal class Converter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(TypeEnum) || t == typeof(TypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(TypeEnum))
                return TypeEnumExtensions.ReadJson(reader, serializer);
            if (t == typeof(TypeEnum?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return TypeEnumExtensions.ReadJson(reader, serializer);
            }
            throw new Exception("Unknown type");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
            if (t == typeof(TypeEnum))
            {
                ((TypeEnum)value).WriteJson(writer, serializer);
                return;
            }
            throw new Exception("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new Converter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
